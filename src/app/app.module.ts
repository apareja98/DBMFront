import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { ArtistsModule } from './artists/artists.module';
import { ArtistaDetalleComponent } from './artista-detalle/artista-detalle.component';
import { CaratulaComponent } from './caratula/caratula.component';
import { CancionComponent } from './cancion/cancion.component';

@NgModule({
  declarations: [
    AppComponent,
    ArtistaDetalleComponent,
    CaratulaComponent,
    CancionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ArtistsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
