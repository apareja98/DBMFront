import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { ArtistService } from '../artists/model/artist.service';
@Component({
  selector: 'app-caratula',
  templateUrl: './caratula.component.html',
  styleUrls: ['./caratula.component.css']
})
export class CaratulaComponent implements OnInit {
  @Input() id:any;
  base64 :any;
  constructor(private as:ArtistService) { 
   
   
  }
  ngAfterViewInit(){
    console.log(this.id)
    this.as.getCaratula(this.id.toString()).subscribe(response =>{this.base64=response;
    this.base64= this.base64.base64});

    
  }
  ngOnInit() {
   
      }

}
