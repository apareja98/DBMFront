import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { ArtistService } from '../artists/model/artist.service';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
@Component({
  selector: 'app-artista-detalle',
  templateUrl: './artista-detalle.component.html',
  styleUrls: ['./artista-detalle.component.css']
})
export class ArtistaDetalleComponent implements OnInit {
  artistID: string;
  artista : any;
  canciones : any[];
  constructor(private route: ActivatedRoute, private artistService : ArtistService) {
   
  
   }

  ngOnInit() {
    
    this.artistID = this.route.snapshot.params['id'];
    console.log(this.artistID);
    this.artistService.getArtist(this.artistID).subscribe(response =>{this.artista=response});
    this.artistService.getSongsByArtist(this.artistID).subscribe(r=>{this.canciones=r;
    console.log(this.canciones)})
    
  }

}
