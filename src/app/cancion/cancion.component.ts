import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { ArtistService } from '../artists/model/artist.service';

@Component({
  selector: 'app-cancion',
  templateUrl: './cancion.component.html',
  styleUrls: ['./cancion.component.css']
})
export class CancionComponent implements OnInit {
  songId: string;
  name : string;
  song:any;
  letra:any;
  base64:any;
  album:any;
  constructor(private route: ActivatedRoute, private artistService : ArtistService) { 
    
  }

  ngOnInit() {
    this.songId = this.route.snapshot.params['id'];
    console.log(this.songId);
    this.name = this.route.snapshot.params['name'];
    this.album = this.route.snapshot.params['idalbum'];
    this.artistService.getCaratula(this.album).subscribe(response =>{this.base64=response;
      this.base64= this.base64.base64;
    console.log(response  )});
    this.artistService.getSong(this.songId).subscribe(r=>{
      this.letra=r;
      console.log(this.letra);
    })
    this.artistService.getRingtone(this.songId,this.name).subscribe(r=>{
      this.song =r;
      console.log(r);
    })
  }

}
