export class Artist {
    artistCod: number;
    artistName: string;
    genre: string;
    biografy: string;
    image: File;
}
