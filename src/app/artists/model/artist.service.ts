import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Artist } from './artist';
import { Key } from 'protractor';

@Injectable()
export class ArtistService {

  resourceURL = 'http://localhost:3000/api/artists';

  constructor(private http: HttpClient) { }

  getArtists(): Observable<Artist[]>{
    //get Artists from api server
    return this.http.get<Artist[]>(this.resourceURL);
  }
  getArtist(id){
    //get Artists from api server
    return this.http.get(this.resourceURL+ '/'+id);
  }
  getSongsByArtist(id):any{
    return this.http.get("http://localhost:3000/api/songs/"+id);
  }
  getRingtone(id,name){
    return this.http.get("http://localhost:3000/api/songs/ringtone/"+id+"/"+name);
  }
  
  addArtist(artist: Artist): Observable<Artist> {
    const fd = new FormData();
    Object.keys(artist).forEach(key => {
      fd.append(key, artist[key]);
    })
    return this.http.post<Artist>(this.resourceURL, fd);
  } 
  getCaratula(id){
    return this.http.get("http://localhost:3000/api/songs/caratula/"+id);
  }

  getSong(idSong){
    return this.http.get("http://localhost:3000/api/songs/info/"+idSong);
  }

}
