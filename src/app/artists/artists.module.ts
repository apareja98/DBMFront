import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ArtistsRoutingModule } from './artists-routing.module';
import { ArtistListComponent } from './artist-list/artist-list.component';
import { ArtistService } from './model/artist.service';
import { ArtistDetailComponent } from './artist-detail/artist-detail.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ArtistsRoutingModule
  ],
  declarations: [ArtistListComponent, ArtistDetailComponent],
  providers: [ArtistService]
})
export class ArtistsModule { }
