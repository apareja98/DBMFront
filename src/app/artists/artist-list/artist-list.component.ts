import { Component, OnInit } from '@angular/core';
import { Artist } from '../model/artist';
import { ArtistService } from '../model/artist.service';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser'
@Component({
  selector: 'app-artist-list',
  templateUrl: './artist-list.component.html',
  styleUrls: ['./artist-list.component.css']
})
export class ArtistListComponent implements OnInit {

  artists: any[]

  constructor(private artistService: ArtistService,private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.artistService.getArtists()
    .subscribe(artists => {
      this.artists = artists
      for(var idx=0; idx<this.artists.length; idx++){
        this.artists[idx].base64 =this.sanitizer.bypassSecurityTrustUrl("data:image/png;base64,"+this.artists[idx].base64);
      }
    });
  }

}
