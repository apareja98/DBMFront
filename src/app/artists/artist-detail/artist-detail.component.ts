import { Component, OnInit } from '@angular/core';
import { ArtistService } from '../model/artist.service';
import { Artist } from '../model/artist';
import { Router } from '@angular/router';

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.css']
})
export class ArtistDetailComponent implements OnInit {

  title: string;
  artist: Artist;
  constructor(private artistService: ArtistService,
    private router: Router) { }

  ngOnInit() {
    this.title = 'Artist Create';
    this.artist = new Artist;
  }

  save() {
    this.artistService.addArtist(this.artist)
    .subscribe(res => this.router.navigate(['']));
  }

  handleFileInput(files: FileList){
    this.artist.image = files.item(0);
  }

}
