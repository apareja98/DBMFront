import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArtistDetailComponent } from './artist-detail/artist-detail.component';
import {ArtistaDetalleComponent} from '../artista-detalle/artista-detalle.component';
import {CancionComponent} from '../cancion/cancion.component';
const routes: Routes = [  
  {path: 'artist-create', component: ArtistDetailComponent},
  {path:'detail/:id', component:ArtistaDetalleComponent},
  {path:'song/:id/:idalbum/:name', component:CancionComponent}
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtistsRoutingModule { }
